# acme-cli

Command line utility for issuing certificates using ACME, 
supporting both LetsEncrypt and ZeroSSL. Verification
is done using DNS with the CloudFlare API.

## Prebuilt binaries

The following (x86_64) platforms have prebuilt binaries from the CI:

* [Linux](https://gitlab.com/deftware/acme-cli/builds/artifacts/master/raw/target/release/acme-cli?job=linux)
* [Windows](https://gitlab.com/deftware/acme-cli/builds/artifacts/master/raw/target/x86_64-pc-windows-gnu/release/acme-cli.exe?job=windows)

## Usage

Provided there is a `config.yaml` file in the current directory (see config.yaml.example),
the tool can be run using `./acme-cli --domain <your fqdn>`. The certificates will be
generated in a `domains/<fqdn>/` directory.

