use base64::Engine;
use clap::Parser;
use std::{
    fs::{create_dir_all, write, File},
    io::BufReader,
    path::Path,
    time::Duration,
};

use cloudflare::framework::{async_api::Client, HttpApiClientConfig};
use cloudflare::framework::auth::Credentials;
use cloudflare::framework::Environment;
use cloudflare::endpoints::dns::{CreateDnsRecord, CreateDnsRecordParams, DeleteDnsRecord, DnsContent};

use anyhow::{anyhow, Result};
use base64::engine::general_purpose::URL_SAFE_NO_PAD;
use instant_acme::{
    Account, AuthorizationStatus, ChallengeType, ExternalAccountKey, Identifier, LetsEncrypt,
    NewAccount, NewOrder, Order, OrderStatus, ZeroSsl,
};
use log::{debug, info};
use rcgen::{CertificateParams, DistinguishedName, KeyPair};
use serde::Deserialize;
use serde_yaml::from_reader;
use tokio::time::sleep;

#[derive(Debug, Deserialize)]
struct ExternalAccount {
    /// External account hmac in URL safe formatting (+/ to -_)
    hmac: String,
    /// External account id
    id: String,
}

#[derive(Debug, Deserialize)]
enum Provider {
    Staging,
    LetsEncrypt,
    ZeroSSL,
}

#[derive(Debug, Deserialize)]
struct Domain {
    /// The FQDN
    fqdn: String,
    /// CloudFlare account token
    cf_token: String,
    /// CloudFlare Zone ID
    cf_zone: String,
    /// Optional external account authentication
    account: Option<ExternalAccount>,
    /// Contact url
    contact: String,
    /// ACME provider
    provider: Provider,
}

#[derive(Debug, Deserialize)]
struct Config {
    domains: Vec<Domain>,
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Domain to generate SSL certificate for
    #[arg(short, long)]
    domain: String,
}

async fn get_account(domain: &Domain, domain_dir: &Path) -> Result<Account> {
    let external_account = match &domain.account {
        Some(account) => {
            debug!("Using external account authentication");
            let data = URL_SAFE_NO_PAD.decode(&account.hmac)?;
            Some(ExternalAccountKey::new(account.id.to_string(), &data))
        }
        None => None,
    };

    let provider = match domain.provider {
        Provider::ZeroSSL => ZeroSsl::Production.url(),
        Provider::LetsEncrypt => LetsEncrypt::Production.url(),
        Provider::Staging => LetsEncrypt::Staging.url(),
    };
    info!("Using {provider}");

    let account_file = domain_dir.join("account.json");
    let account = match account_file.exists() {
        true => {
            info!("Using existing account");
            let reader = BufReader::new(File::open(account_file)?);
            let credentials = serde_json::from_reader(reader)?;
            Account::from_credentials(credentials).await?
        }
        false => {
            info!("Creating new account...");
            let (account, credentials) = Account::create(
                &NewAccount {
                    contact: &[&domain.contact],
                    terms_of_service_agreed: true,
                    only_return_existing: false,
                },
                provider,
                external_account.as_ref(),
            )
            .await?;
            let json = serde_json::to_string_pretty(&credentials)?;
            write(account_file, json)?;
            account
        }
    };
    Ok(account)
}

async fn wait_for_order(order: &mut Order) -> Result<()> {
    let mut tries = 1u8;
    let mut delay = Duration::from_millis(250);
    loop {
        sleep(delay).await;
        let state = order.refresh().await?;
        if let OrderStatus::Ready | OrderStatus::Invalid = state.status {
            break;
        }

        delay *= 2;
        tries += 1;

        if tries >= 10 {
            return Err(anyhow!("order is not ready"));
        }
        info!("Order is not ready, waiting {delay:?}");
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init_from_env(
        env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "info"),
    );

    let args = Args::parse();

    let file = File::open("./config.yaml")?;
    let reader = BufReader::new(file);
    let config: Config = from_reader(reader)?;

    let domain = config
        .domains
        .iter()
        .find(|value| value.fqdn == args.domain)
        .ok_or_else(|| anyhow!("Could not find domain {}", args.domain))?;
    info!("Using domain {}", domain.fqdn);

    let domain_dir = Path::new("./domains").join(&domain.fqdn);
    if !domain_dir.exists() {
        create_dir_all(&domain_dir)?;
    }
    let account = get_account(domain, &domain_dir).await?;

    let creds = Credentials::UserAuthToken {
        token: domain.cf_token.to_string(),
    };
    let client = Client::new(creds, 
            HttpApiClientConfig::default(), 
            Environment::Production).expect("A CloudFlare client");

    // Create order
    info!("Creating order...");
    let identifier = Identifier::Dns(domain.fqdn.to_string());
    let mut order = account
        .new_order(&NewOrder {
            identifiers: &[identifier],
        })
        .await?;

    // Based on https://github.com/instant-labs/instant-acme/blob/main/examples/provision.rs

    let authorizations = order.authorizations().await?;
    let auth = authorizations
        .first()
        .ok_or_else(|| anyhow!("Could not find any authorization"))?;
    let Identifier::Dns(identifier) = &auth.identifier;

    if matches!(auth.status, AuthorizationStatus::Pending) {
        let challenge = auth
            .challenges
            .iter()
            .find(|c| c.r#type == ChallengeType::Dns01)
            .ok_or_else(|| anyhow!("No DNS challenge found"))?;

        let name = format!("_acme-challenge.{identifier}");
        let dns_value = order.key_authorization(challenge).dns_value();

        // Create record
        info!("Creating TXT DNS record {name} => {dns_value}");
        let params = CreateDnsRecordParams {
            name: &name,
            content: DnsContent::TXT { content: dns_value },
            priority: None,
            proxied: Some(false),
            ttl: Some(1),
        };
        let req = CreateDnsRecord {
            params,
            zone_identifier: &domain.cf_zone,
        };
        let response = match client.request(&req).await {
            Ok(val) => val.result,
            Err(err) => return Err(anyhow!("Unable to create DNS record {err}")),
        };
        // Sleep some to allow DNS change to take effect
        sleep(Duration::from_secs(5)).await;
        // Notify ACME server we can accept challenges
        order.set_challenge_ready(&challenge.url).await?;

        info!("Waiting for DNS challenge to be completed");
        let result = wait_for_order(&mut order).await;

        // Delete DNS record
        info!("Deleting DNS record {name}");
        let req = DeleteDnsRecord {
            identifier: &response.id,
            zone_identifier: &domain.cf_zone,
        };
        if let Err(err) = client.request(&req).await {
            return Err(anyhow!("Unable to delete DNS record {err}"));
        }

        // Check result here to ensure we always delete the DNS record, 
        // even if the DNS challenge failed
        if let Err(err) = result {
            return Err(err)
        }
    }

    let state = order.state();
    if state.status != OrderStatus::Ready {
        return Err(anyhow!("Unexpected order status: {:?}", state.status));
    }

    // Create certificate request
    info!("Creating certificate request");
    let names = vec![identifier.to_owned()];
    let mut params = CertificateParams::new(names)?;
    params.distinguished_name = DistinguishedName::new();
    let private_key = KeyPair::generate()?;
    let csr = params.serialize_request(&private_key)?;

    // Finalize order
    info!("Finalizing order");
    order.finalize(csr.der()).await?;
    let cert_chain_pem = loop {
        match order.certificate().await? {
            Some(cert_chain_pem) => break cert_chain_pem,
            None => sleep(Duration::from_secs(1)).await,
        }
    };

    // Write to disk
    let cert_dir = domain_dir.join("certificate");
    info!("Writing certificate to {:?}", cert_dir);
    if !cert_dir.exists() {
        create_dir_all(&cert_dir)?;
    }
    write(cert_dir.join("certificate.key"), private_key.serialize_pem())?;
    write(cert_dir.join("certificate.crt"), cert_chain_pem)?;

    Ok(())
}
